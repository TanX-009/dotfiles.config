#!/bin/bash

# Check if the correct number of parameters are passed
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <directory> <command>"
	exit 1
fi

# Assign the parameters to variables
TARGET_DIR="$1"
COMMAND="$2"

# Check if the first parameter is a directory
if [ ! -d "$TARGET_DIR" ]; then
	echo "Error: $TARGET_DIR is not a directory"
	exit 1
fi

# Change to the target directory
cd "$TARGET_DIR" || exit

# List all the folders in the target directory
# echo "Folders in $TARGET_DIR:"
select folder in */; do
	if [ -n "$folder" ]; then
		# Remove the trailing slash from the folder name
		folder="${folder%/}"

		# Change to the selected folder
		cd "$folder" || exit

		# Run the specified command
		$COMMAND

		# Exit the script
		exit 0
	else
		echo "Invalid selection. Please try again."
	fi
done
