#!/usr/bin/env fish

# Check if the correct number of parameters are passed
if test (count $argv) -ne 2
    echo "Usage: $argv[0] <directory> <command>"
    return 1
end

# Assign the parameters to variables
set TARGET_DIR $argv[1]
set COMMAND $argv[2]

# Check if the first parameter is a directory
if not test -d $TARGET_DIR
    echo "Error: $TARGET_DIR is not a directory"
    return 1
end

# Change to the target directory
cd $TARGET_DIR

# List all the folders in the target directory
echo "Folders in $TARGET_DIR:"
set folders (ls -d */)

for i in (seq (count $folders))
    echo "$i) $folders[$i]"
end

# Prompt the user to select a folder
echo "Select a folder:"
read -l selection

# Validate selection
if test "$selection" -gt 0 -a "$selection" -le (count $folders)
    # Get the selected folder and remove trailing slash
    set selected_folder $folders[$selection]
    set selected_folder (string trim -r "/" $selected_folder)
    
    # Change to the selected folder
    cd "$selected_folder"

    # Run the specified command
    eval $COMMAND
else
    echo "Invalid selection. Exiting."
    return 1
end
